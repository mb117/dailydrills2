let users = require('./p2-1.js');

//{"id":1,"first_name":"Valera","last_name":"Pinsent","email":"vpinsent0@google.co.jp",
//"gender":"Male","ip_address":"253.171.63.171"

// 1. Find all people who are Agender

function findAgender(users) {
    let results = users.filter(function( user) {
        if ( user.gender === "Agender") {
            return true;
        }
        return false;
    });

    return results;
}

// console.log(findAgender(users));

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
function splitIP( users) {
    let result = users.map(function(user) {
        return user.ip_address.split(".");
    });

    return result;
}
// console.log(splitIP(users));

// 3. Find the sum of all the second components of the ip addresses.

function sumSecondIPComponent(users) {
    let result = users.reduce(function(previousIP, user) {
        return previousIP + Number(user.ip_address.split(".")[1]);
    }, 0);

    return result;
}

// console.log(sumSecondIPComponent(users));

// 3. Find the sum of all the fourth components of the ip addresses.
function sumFourthIPComponent(users) {
    let result = users.reduce(function(previousIP, user) {
        return previousIP + Number(user.ip_address.split(".")[3]);
    }, 0);

    return result;
}

// console.log(sumFourthIPComponent(users));

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
function fullName(users) {
    let usersWithFullName = [];

    usersWithFullName = users.map(function(user) {
        user["full_name"] = user.first_name + " " + user.last_name;

        return user;
    });
    return usersWithFullName;
}

// console.log(fullName(users));

// 5. Filter out all the .org emails
function filterORG(users) {
    let result = [];

    result = users.filter(function(user) {
        // removing if the emails contain .org
        return !(user.email.includes(".org"))
    });
    
    return result;
}

console.log(filterORG(users));

// 6. Calculate how many .org, .au, .com emails are there
function countDomains(users) {
    let result = [];

    result = users.filter(function(user) {
        return user.email.includes(".org") || user.email.includes(".au") || user.email.includes(".com");
    });

    return result.length;
}

console.log(countDomains(users));

// 7. Sort the data in descending order of first name
function sortByFirstName(users) {
    users.sort(function(a,b) {
        if ( a.first_name < b.first_name) {
            return -1;
        } else if ( a.first_name > b.first_name) {
            return 1;
        } else {
            return 0;
        }
    });
    return users;
}

// console.log(sortByFirstName(users));